<?php

namespace Bestxp\Widget\Silex;

use Bestxp\Widget\Twig\Widget as WidgetExtension;
use Silex\Application;
use Silex\ServiceProviderInterface;

class WidgetServiceProvider implements ServiceProviderInterface{

    public function register(Application $app)
    {
        if($app->offsetExists('twig')){
            $app['twig.widget'] = $app->share(function($app){
                return new WidgetExtension($app);
            });
            $app['twig'] = $app->share($app->extend('twig', function($twig, $app){
                $twig->addExtension($app['twig.widget']);
                return $twig;
            }));
        }
    }

    public function boot(Application $app)
    {
        // TODO: Implement boot() method.
    }

}
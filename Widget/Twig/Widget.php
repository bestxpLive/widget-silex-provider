<?php
namespace Bestxp\Widget\Twig;

use Silex\Application;

class Widget extends \Twig_Extension
{

    protected $container;

    protected $widgets = [];

    public function __construct(Application $app)
    {
        $this->container = $app;
    }

    public function addWidget($alias, $callable)
    {
        $this->widgets[$alias] = $callable;
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction($this->getName(), [$this, 'getWidget'])
        ];
    }

    public function getWidget($widgetName)
    {

        $args = func_get_args();
        array_shift($args); //remove $widgetName;

        if (array_key_exists($widgetName, $this->widgets)) {
            $widgetName = $this->widgets[$widgetName];
        } else {
            throw new \Twig_Error_Runtime('Widget "' . $widgetName . '" doesn\'t exists');
        }

        if (($widgetName instanceof \Closure)) {
            return call_user_func_array($widgetName, $args);
        }

        $class = new \ReflectionClass($widgetName);
        if ($class->isSubclassOf(\Bestxp\Widget\Base\Widget::class)) {
            $w = $class->newInstance($this->container);
            return call_user_func_array($w, $args);
        }

    }


    public function getName()
    {
        return 'widget';
    }


}
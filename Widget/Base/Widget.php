<?php

namespace Bestxp\Widget\Base;

use Silex\Application;

class Widget{
    private $app;



    public function __construct(Application $app)
    {
        $this->app = $app;
        if(!method_exists(static::class, '__invoke')){
            throw new \Twig_Error_Runtime(get_called_class() . ' is not widget. Please implement __invoke method.');
        }
    }
}